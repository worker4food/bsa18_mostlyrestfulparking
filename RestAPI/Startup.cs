﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Converters;
using NJsonSchema;
using NSwag.AspNetCore;
using System.Reflection;
using ParkingLib;

namespace RestAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(o => 
                    o.SerializerSettings.Converters.Add(new StringEnumConverter()));
            
            var settings = new Settings();
            Configuration.Bind("Settings", settings);

            var prices = new Dictionary<CarType, decimal>();
            Configuration.Bind("Settings:Prices_", prices);

            foreach(var (k, v) in prices)
                settings.Prices[k] = v;

            services.AddSingleton<Parking>(_ => new Parking(settings));
         
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseSwaggerUi(typeof(Startup).GetTypeInfo().Assembly, settings => {
                settings.GeneratorSettings.DefaultPropertyNameHandling = 
                    PropertyNameHandling.CamelCase;
                settings.PostProcess = doc => {
                    doc.Info.Title = "Parking API";
                };
            });
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMvc();
        }
    }
}
