using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.AspNetCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ParkingLib;

namespace RestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/cars")]
    public class CarsController : Controller
    {
        public class CarReq {
            [JsonConverter(typeof(StringEnumConverter))]
            public CarType Type {get; set;}
            public decimal Balance {get; set;}
        }
        private Parking _parking;

        public CarsController(Parking parking) {
            _parking = parking;
        }

        // GET: api/Cars
        [HttpGet]
        public IEnumerable<Car> Get() {
            return _parking.Cars;
        }

        // GET: api/Cars/5
        [HttpGet("{id}")]
        [ProducesResponseType(404)]
        public Car Get(string id) {
            return _parking.GetCar(id);
        }
        
        // POST: api/Cars
        [HttpPost]
        [ProducesResponseType(402)]
        [ProducesResponseType(423)]
        public string Post([FromBody] CarReq c) {
            return _parking.AddCar(c.Type, c.Balance);
        }
        
        // DELETE: api/cars/5
        [HttpDelete("{id}")]
        [ProducesResponseType(402)]
        [ProducesResponseType(404)]
        public Car Delete(string id) {
            return _parking.RemoveCar(id);
        }
    }
}
