using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingLib;

namespace RestAPI.Controllers {
    [Produces("application/json")]
    [Route("api/transactions")]
    public class TransactionsController : Controller
    {
        private Parking _parking;

        public TransactionsController(Parking p) {
            _parking = p;
        }

        // GET: api/Transactions
        [HttpGet]
        public IEnumerable<Transaction> Get() 
            => _parking.Transactions;

        [HttpGet("last")]
        public IEnumerable<Transaction> GetLatest() {
            var currTS = DateTime.Now;
            return _parking.Transactions
                .TakeWhile(t => (currTS - t.Timestamp).TotalMinutes <= 1);
        }

        // GET: api/Transactions/5
        [HttpGet("last/{id}")]
        public IEnumerable<Transaction> Get(string id) {
            var currTS = DateTime.Now;
            return _parking.Transactions //TODO: Need some more efficient
                    .Where(t => t.CarId == id && (currTS - t.Timestamp).TotalMinutes <= 1);
        }
        
        // PUT: api/Transactions/5
        [HttpPut("{id}")]
        [ProducesResponseType(404)]
        public Transaction Put(string id, [FromBody]decimal value)
            =>  _parking.AddToCarDeposite(id, value);
    }
}
