using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingLib;

namespace RestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/parking")]
    public class ParkingController : Controller
    {
        private Parking _parking;

        public ParkingController(Parking p) {
            _parking = p;
        }

        // GET: api/parking/freeplaces
        [HttpGet("freespace")]
        public int FreeSpace() {
            return _parking.FreeSpace;
        }

        // GET: api/parking/balance
        [HttpGet("balance")]
        public decimal Balance() {
            return _parking.Balance;
        }

        // GET: api/parking/carcount
        [HttpGet("carcount")]
        public int CarCount() {
            return _parking.Cars.Count();
        }
    }
}
