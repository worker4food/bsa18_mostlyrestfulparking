# Mostly RESTFull Parking (.net core 2.0)

## Howto launch this app
1. Checkout repo
2. `dotnet run --project RestAPI`
3. Open url http://localhost:5000/swagger/

### Configuration in appsettings.json
```
...
  "Settings": {
    "ParkingSpace": 15,
    "FineFactor": 2.5,
    "TimeOut": "0.00:00:10", // D.HH.mm.ss
    "Prices_": {
      "Truck":      50,
      "Passenger":  30,
      "Bus":        20,
      "Motorcycle": 100
    }
  }
...
```