using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Timers;

namespace ParkingLib {
    public class Parking {
        private ConcurrentDictionary<string, Car> _cars = new ConcurrentDictionary<string, Car>();
       
        private ConcurrentDictionary<string, Timer> _carTimers = new ConcurrentDictionary<string, Timer>();
       
        private Settings _settings;

        public IEnumerable<Car> Cars => _cars.AsEnumerable().Select(e => e.Value);

        private ConcurrentStack<Transaction> _transactions = new ConcurrentStack<Transaction>();
       
        public IEnumerable<Transaction> Transactions => _transactions.AsEnumerable();

        public int FreeSpace => _settings.ParkingSpace - _cars.Count;

        public decimal Balance { get; private set; }

        public Parking(Settings settings) {
            _settings = settings;
        }
       
        protected void AddCarAndTimer(Car c, Timer t) {
            Car _c;
            Timer _t;
            try {
                if(!(_cars.TryAdd(c.Id, c) && _carTimers.TryAdd(c.Id, t)))
                    throw new ArgumentException("Error adding car or timer!");
            }
            catch(ArgumentException) { // Undo ALL
                _cars. TryRemove(c.Id, out _c);
                _carTimers.TryRemove(c.Id, out _t);

                throw;
            }
        }
        public string AddCar(CarType type, decimal carBalance) {

            if(carBalance <= 0)
                throw new NoMoneyException();

            if(FreeSpace <= 0)
                throw new ParkingIsFullException();

            var car = new Car(Settings.NewId(), type, carBalance);
            var timer = new Timer();

            AddCarAndTimer(car, timer);

            timer.Interval = _settings.Timeout.TotalMilliseconds;
            timer.Elapsed += (_1, _2) => ProcessCar(car);
            timer.Enabled = true;

            return car.Id;
        }

        public Car RemoveCar(string id) {
            try {
                Car c = _cars[id], _c;
                Timer t = _carTimers[id], _t;

                t.Enabled = false;

                lock(c) {
                    if(c.Balance < 0) {
                        t.Enabled = true;
                        throw new NoMoneyException(id);
                    }

                    _cars.TryRemove(id, out _c);
                    _carTimers.TryRemove(id, out _t);

                    return c;
                }
            }
            catch(KeyNotFoundException) {
                throw new CarNotFoundException();
            }
        }

        public Car GetCar(string id){
            try {
                return _cars[id];
            }
            catch(KeyNotFoundException) {
                throw new CarNotFoundException();
            }
        }

        public Transaction AddToCarDeposite(string id, decimal sum) {
            try {
                var car = _cars[id];
                lock(car) {
                    return car.AddToDeposite(sum);
                }
            }
            catch(KeyNotFoundException) {
                throw new CarNotFoundException();
            }
        }

        private void ProcessCar(Car c) {
            Transaction t;
            lock(c) {
                t = c.MakePayment(_settings.Prices[c.Type], _settings.FineFactor);
            }
            
            _transactions.Push(t);
            
            lock(this) {
                Balance += t.Sum;
            }
        }
    }
}
