﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace ParkingLib
{
    public class Settings {
        public TimeSpan Timeout { get; set;} = TimeSpan.FromSeconds(15);
        public int ParkingSpace { get; set;} = 42;
        public decimal FineFactor { get; set;} = 1.3m;
        public Dictionary<CarType, decimal> Prices { get; } = new Dictionary<CarType, decimal> {
                    {CarType.Truck,      5},
                    {CarType.Passenger,  3},
                    {CarType.Bus,        2},
                    {CarType.Motorcycle, 1}
                };

        public Settings() {}

        public Settings(TimeSpan timeout, int parkSpace, decimal fineFactor, IDictionary<CarType, decimal> prices) {
            Timeout = timeout;
            ParkingSpace = parkSpace;
            FineFactor = fineFactor;
            Prices = new Dictionary<CarType, decimal>(prices);
        }

        public static string NewId() => Guid.NewGuid().ToString();
    }
}
