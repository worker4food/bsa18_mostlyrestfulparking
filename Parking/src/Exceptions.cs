using System;

namespace ParkingLib {
    public class NoMoneyException : Exception {
        public NoMoneyException(string carId): base(String.Format("No money left for car: {0}", carId)) {}
        public NoMoneyException(): base("No money - no parking") {}
    }

    public class ParkingIsFullException : Exception {}

    public class CarNotFoundException : Exception {
        public CarNotFoundException(): base("Car not found") {}
    };
}
