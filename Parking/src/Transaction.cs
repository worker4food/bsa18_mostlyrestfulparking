using System;
using System.Text;

namespace ParkingLib {
    public class Transaction {
        public DateTime Timestamp { get; }
        public string CarId { get; }
        public decimal Sum { get; }

        public Transaction(string carId, decimal sum) {
            Timestamp = DateTime.Now;
            CarId = carId;
            Sum = sum;
        }
    }
}
